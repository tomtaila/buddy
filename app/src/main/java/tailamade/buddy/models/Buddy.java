package tailamade.buddy.models;

/**
 * Created by tomtaila on 5/11/16.
 */
public class Buddy {

    public static final int STATE_SENT = 0;
    public static final int STATE_RECEIVED = 1;
    public static final int STATE_ACCEPTED = 2;

    private String uid;
    private String buddyUid;
    private int state;

    public Buddy(){}

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getBuddyUid() {
        return buddyUid;
    }

    public void setBuddyUid(String buddyUid) {
        this.buddyUid = buddyUid;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
