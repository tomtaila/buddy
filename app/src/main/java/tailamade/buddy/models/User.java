package tailamade.buddy.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by tomtaila on 4/2/16.
 */
public class User implements Parcelable {
    private String uid;
    private String name;
    private String profilePic;
    private String provider;
    private int age;
    private String bio;
    private int rating;
    private int numRatings;

    public User() {
    }

    protected User(Parcel in) {
        uid = in.readString();
        name = in.readString();
        profilePic = in.readString();
        provider = in.readString();
        age = in.readInt();
        bio = in.readString();
        rating = in.readInt();
        numRatings = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uid);
        dest.writeString(name);
        dest.writeString(profilePic);
        dest.writeString(provider);
        dest.writeInt(age);
        dest.writeString(bio);
        dest.writeInt(rating);
        dest.writeInt(numRatings);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getNumRatings() {
        return numRatings;
    }

    public void setNumRatings(int numRatings) {
        this.numRatings = numRatings;
    }

    public String ratingToString() {
        if(numRatings == 0) return "No reviews yet";
        else {
            return "Rating = " + rating;
        }
    }
}