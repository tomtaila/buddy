package tailamade.buddy;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import tailamade.buddy.databinding.ItemBuddyBinding;
import tailamade.buddy.models.Buddy;
import tailamade.buddy.models.User;

/**
 * Created by tomtaila on 5/11/16.
 */
public class BuddyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "BuddyAdapter";

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_BUDDY = 1;

    private static final String HEADER_REQUESTED = "Requested";
    private static final String HEADER_SENT = "Sent";
    private static final String HEADER_BUDDIES = "Buddies";

    private List<Buddy> buddies;
    private List<Buddy> requestsSent;
    private List<Buddy> requestsReceived;

    private Firebase ref = new Firebase(Constants.FIREBASE_BASE);

    public BuddyAdapter(List<Buddy> requestsReceived, List<Buddy> requestsSent, List<Buddy> buddies) {
        this.requestsReceived = requestsReceived;
        this.requestsSent = requestsSent;
        this.buddies = buddies;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_BUDDY){
            ItemBuddyBinding buddyBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.item_buddy,
                    parent,
                    false
            );

            return new BuddyViewHolder(buddyBinding);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header, parent, false);
            return new HeaderViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0 || position == requestsReceived.size()+1 || position == requestsReceived.size()+requestsSent.size()+2) return TYPE_HEADER;
        else return TYPE_BUDDY;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_BUDDY:
                final BuddyViewHolder buddyViewHolder = (BuddyViewHolder) holder;
                Buddy buddy;
                if(position <= requestsReceived.size()) {
                    buddy = requestsReceived.get(position-1);
                } else if(position <= requestsReceived.size()+requestsSent.size()+1) {
                    buddy = requestsSent.get(position-requestsReceived.size()-2);
                } else {
                    buddy = buddies.get(position-requestsReceived.size()-requestsSent.size()-3);
                }
                buddyViewHolder.binding.setBuddy(buddy);
                ref.child(Constants.FIREBASE_CHILD_USERS).child(buddy.getBuddyUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        buddyViewHolder.binding.setUser(user);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {
                        Log.d(TAG, "onCancelled: " + firebaseError.getMessage());
                    }
                });
                break;
            case TYPE_HEADER:
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                if(position == 0) {
                    headerViewHolder.tvHeader.setText(HEADER_REQUESTED);
                } else if(position == requestsReceived.size()+1) {
                    headerViewHolder.tvHeader.setText(HEADER_SENT);
                } else if(position == requestsReceived.size()+requestsSent.size()+2) {
                    headerViewHolder.tvHeader.setText(HEADER_BUDDIES);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return requestsReceived.size()+requestsSent.size()+buddies.size()+3;
    }

    public class BuddyViewHolder extends RecyclerView.ViewHolder {

        ItemBuddyBinding binding;

        public BuddyViewHolder(ItemBuddyBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_header)
        TextView tvHeader;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
