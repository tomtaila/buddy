package tailamade.buddy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.OnClick;
import tailamade.buddy.models.User;

public class LoginActivity extends AppCompatActivity {

    private List<String> fbLoginPermissions;
    private CallbackManager fbCallbackManager;
    private AccessTokenTracker fbAccessTokenTracker;
    private User user;

    private static final String TAG = "LoginActivity";

    private Firebase ref = new Firebase(Constants.FIREBASE_BASE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        setupFacebookAuth();

        if(sessionExists()) {
            generateUserFromAuthData(ref.getAuth());
        }
    }

    private boolean sessionExists() {
        return ref.getAuth() != null;
    }

    @OnClick(R.id.btn_fb_login) void loginWithFb() {
        Log.d(TAG, "FB login btn clicked!");
        LoginManager.getInstance().logInWithReadPermissions(this, fbLoginPermissions);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // if user logged in with Facebook, stop tracking their token
        if (fbAccessTokenTracker != null) {
            fbAccessTokenTracker.stopTracking();
        }
    }

    private void setupFacebookAuth() {
        fbLoginPermissions = new ArrayList<>();
        fbLoginPermissions.add("public_profile");

        fbCallbackManager = CallbackManager.Factory.create();
        fbAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                Log.d(TAG, "Facebook.AccessTokenTracker.OnCurrentAccessTokenChanged");
                LoginActivity.this.onFacebookAccessTokenChange(currentAccessToken);
            }
        };

        LoginManager.getInstance().registerCallback(fbCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "FB login : nSuccess");
                onFacebookAccessTokenChange(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "FB login : onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "FB login : onError : " + error.toString());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fbCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void onFacebookAccessTokenChange(AccessToken token) {
        if (token != null) {
            ref.authWithOAuthToken("facebook", token.getToken(), new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    // The Facebook user is now authenticated with your Firebase app
                    Log.d(TAG, "Facebook user now authenticated with firebase");
                    storeUserData(authData);
                    generateUserFromAuthData(authData);
                }

                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    // there was an error
                    Log.d(TAG, "Error whilst trying to authenticate Facebook user with firebase. Error = " + firebaseError.getMessage());
                }
            });
        } else {
        /* Logged out of Facebook so do a logout from the Firebase app */
            ref.unauth();
        }
    }

    private void storeUserData(AuthData authData) {
        Map<String, Object> map = new HashMap<>();
        map.put("provider", authData.getProvider());
        map.put("uid", authData.getUid());
        switch (authData.getProvider()) {
            case Constants.PROVIDER_FB:
                map.put("name", authData.getProviderData().get("displayName").toString());
                map.put("profilePic", authData.getProviderData().get("profileImageURL").toString());
                break;
        }
        Firebase userRef = ref.child(Constants.FIREBASE_CHILD_USERS).child(authData.getUid());
        userRef.updateChildren(map);
    }

    private void generateUserFromAuthData(AuthData authData) {
        user = new User();
        user.setProvider(authData.getProvider());
        user.setUid(authData.getUid());
        Firebase userRef = ref.child(Constants.FIREBASE_CHILD_USERS).child(authData.getUid());
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                user = snapshot.getValue(User.class);
                startProfileActivity();
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }

    private void startProfileActivity() {
        // Start next activity
        BuddyApplication.getInstance().setUser(user);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
