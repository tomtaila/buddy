package tailamade.buddy;

/**
 * Created by tomtaila on 4/1/16.
 */
public class Constants {

    //Firebase
    public static final String FIREBASE_BASE = "https://taila-buddy.firebaseio.com";
    public static final String FIREBASE_CHILD_USERS = "users";
    public static final String FIREBASE_CHILD_LOCATIONS = "locations";
    public static final String FIREBASE_CHILD_BUDDIES = "buddies";

    // AUTH PROVIDERS
    public static final String PROVIDER_FB = "facebook";
    // EXTRAS
    public static final String EXTRA_USER_UID = "userUid";
    public static final String EXTRA_USER = "user";

    // SHARED PREFS
    public static final String SHARED_PREF_FILENAME = "buddySharedPrefs";
    public static final String SHARED_PREF_LOCATION_PROVIDER_KEY = "provider";
    public static final String SHARED_PREF_LATITUDE_KEY = "latitude";
    public static final String SHARED_PREF_LONGITUDE_KEY = "longitude";

    // GLIDE TRANSFORMATIONS
    public static final int GLIDE_TRANSFORMATION_CIRCULAR = 0;
    public static final int GLIDE_TRANSFORMATION_ROUNDED_CORNERS = 1;
}
