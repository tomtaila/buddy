package tailamade.buddy;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.util.Log;

import com.firebase.client.Firebase;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by ttaila on 4/6/16.
 */
public class GoogleApiClientManager implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static GoogleApiClientManager instance;
    private GoogleApiClient apiClient;
    private LocationRequest locationRequest;

    private static final int LOCATION_UPDATE_INTERVAL_MINS = 30;
    private static final String TAG = "GoogleApiClientManager";

    private GoogleApiClientManager() {
    }

    public synchronized void buildApiClient(Context context) {
        apiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public static GoogleApiClientManager getInstance() {
        if (instance == null) instance = new GoogleApiClientManager();
        return instance;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected: ");

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setInterval(DateUtils.MINUTE_IN_MILLIS * LOCATION_UPDATE_INTERVAL_MINS); // Update location 30 minutes

        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, locationRequest, this);
        } catch (SecurityException e) {
            Log.e(TAG, "onConnected: SecurityException = ", e);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: ");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed: connectionResult.errorMessage = " + connectionResult.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged: location = " + location.toString());
        Firebase ref = new Firebase(Constants.FIREBASE_BASE);
        GeoFire geoFireRef = new GeoFire(ref.child(Constants.FIREBASE_CHILD_LOCATIONS).child(BuddyApplication.getInstance().getUser().getUid()));
        geoFireRef.setLocation(BuddyApplication.getInstance().getUser().getUid(), new GeoLocation(location.getLatitude(), location.getLongitude()));
    }

    public GoogleApiClient getApiClient() {
        return apiClient;
    }
}
