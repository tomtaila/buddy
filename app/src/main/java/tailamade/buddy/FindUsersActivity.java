package tailamade.buddy;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQueryEventListener;
import com.firebase.geofire.LocationCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import tailamade.buddy.models.User;

/**
 * Created by tomtaila on 4/14/16.
 */
public class FindUsersActivity extends AppCompatActivity {

    private static final String TAG = "FindUsersActivity";

    private Firebase ref = new Firebase(Constants.FIREBASE_BASE);
    private Firebase usersRef = ref.child(Constants.FIREBASE_CHILD_USERS);
    private GeoFire geoFireRef = new GeoFire(ref.child(Constants.FIREBASE_CHILD_LOCATIONS));

    private List<User> users;
    private UserAdapter adapter;

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_users);
        ButterKnife.bind(this);

        // Create stub users data
        users = new ArrayList<>();
        // Setting up recycler view
        adapter = new UserAdapter(users);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        findUsers();
    }

    public void onUserClick(View view) {
        User user = (User) view.getTag();
        Log.d(TAG, "onUserClick: " + user.getName());
        Intent intent = new Intent(this, UserActivity.class);
        intent.putExtra(Constants.EXTRA_USER, user);
        startActivity(intent);
    }

    private void findUsers() {
        // get all user keys within certain location
        // get all users
        // filter all users by only getting the ones within the location
        geoFireRef.getLocation(BuddyApplication.getInstance().getUser().getUid(), new LocationCallback() {
            @Override
            public void onLocationResult(String key, GeoLocation location) {
                // TODO: Get radius from shared preferences
                geoFireRef.queryAtLocation(location, 4.9).addGeoQueryEventListener(new GeoQueryEventListener() {
                    @Override
                    public void onKeyEntered(String key, GeoLocation location) {
                        Log.d(TAG, "onKeyEntered: " + key);
                        usersRef.child(key).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                users.add(dataSnapshot.getValue(User.class));
                                adapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {

                            }
                        });
                    }

                    @Override
                    public void onKeyExited(String key) {
                        Log.d(TAG, "onKeyExited: " + key);
                    }

                    @Override
                    public void onKeyMoved(String key, GeoLocation location) {
                        Log.d(TAG, "onKeyMoved: " + key);
                    }

                    @Override
                    public void onGeoQueryReady() {
                        Log.d(TAG, "onGeoQueryReady...");
                    }

                    @Override
                    public void onGeoQueryError(FirebaseError error) {
                        Log.d(TAG, "onGeoQueryError: " + error.toString());
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.d(TAG, "onCancelled: " + firebaseError);
            }
        });
    }

}
