package tailamade.buddy;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.firebase.client.Firebase;

import butterknife.ButterKnife;
import butterknife.OnClick;
import tailamade.buddy.databinding.ActivityUserBinding;
import tailamade.buddy.models.Buddy;
import tailamade.buddy.models.User;

/**
 * Created by tomtaila on 5/1/16.
 */
public class UserActivity extends AppCompatActivity {

    private Firebase ref = new Firebase(Constants.FIREBASE_BASE);

    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = getIntent().getParcelableExtra(Constants.EXTRA_USER);
        ActivityUserBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_user);
        binding.setUser(user);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_request)
    public void sendBuddyRequest() {
        Buddy buddy = new Buddy();
        buddy.setBuddyUid(BuddyApplication.getInstance().getUser().getUid());
        buddy.setState(Buddy.STATE_SENT);

        // PUSH BOTH BUDDY REQUESTS TO HAVE THE SAME UID
        // push sent buddy request to firebase
        Firebase sentRef = ref.child(Constants.FIREBASE_CHILD_BUDDIES)
                .child(BuddyApplication.getInstance().getUser().getUid())
                .push();
        buddy.setUid(sentRef.getKey());
        sentRef.setValue(buddy);

        // push received buddy request to firebase
        Buddy buddyTwo = new Buddy();
        buddyTwo.setBuddyUid(user.getUid());
        buddyTwo.setState(Buddy.STATE_RECEIVED);
        buddyTwo.setUid(sentRef.getKey());
        ref.child(Constants.FIREBASE_CHILD_BUDDIES)
            .child(user.getUid())
            .child(buddyTwo.getUid())
            .setValue(buddyTwo);
    }

}
