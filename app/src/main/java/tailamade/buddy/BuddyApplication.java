package tailamade.buddy;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.firebase.client.Firebase;

import tailamade.buddy.models.User;

/**
 * Created by tomtaila on 4/1/16.
 */
public class BuddyApplication extends Application {

    private static BuddyApplication instance;
    private User user;

    public static BuddyApplication getInstance() {
        if(instance == null) instance = new BuddyApplication();
        return instance;
    }

    public BuddyApplication() {}

    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
        FacebookSdk.sdkInitialize(this);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void logout() {
        this.user = null;
    }
}
