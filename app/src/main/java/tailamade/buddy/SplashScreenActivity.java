package tailamade.buddy;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import tailamade.buddy.models.User;

/**
 * Created by tomtaila on 4/9/16.
 */
public class SplashScreenActivity extends AppCompatActivity {

    private static final String TAG = "SplashScreenActivity";

    private Firebase ref = new Firebase(Constants.FIREBASE_BASE);
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if(isLoggedIn()) generateUserFromAuthData(ref.getAuth());
        else startLoginActivity();
    }

    private boolean isLoggedIn() {
        return ref.getAuth() != null;
    }

    private void startLoginActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    private void startMainActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private void generateUserFromAuthData(AuthData authData) {
        user = new User();
        user.setProvider(authData.getProvider());
        user.setUid(authData.getUid());
        Firebase userRef = ref.child(Constants.FIREBASE_CHILD_USERS).child(authData.getUid());
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                user = snapshot.getValue(User.class);
                BuddyApplication.getInstance().setUser(user);
                startMainActivity();
            }
            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        });
    }
}
