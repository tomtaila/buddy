package tailamade.buddy;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import tailamade.buddy.models.Buddy;

// Right now only show the buddy requests received TODO: show buddies accepted and buddy requests sent
public class BuddiesActivity extends AppCompatActivity {

    private List<Buddy> requestsReceived;
    private List<Buddy> requestsSent;
    private List<Buddy> buddies;
    private Firebase ref = new Firebase(Constants.FIREBASE_BASE).child(Constants.FIREBASE_CHILD_BUDDIES);
    private BuddyAdapter adapter;

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_received_requests);
        ButterKnife.bind(this);

        // initialize buddy requests
        initializeBuddies();

        // setup recycler view
        setupRecyclerView();
    }

    private void initializeBuddies() {
        requestsReceived = new ArrayList<>();
        requestsSent = new ArrayList<>();
        buddies = new ArrayList<>();
        ref.child(BuddyApplication.getInstance().getUser().getUid())
            .addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Buddy buddy = dataSnapshot.getValue(Buddy.class);
                    if(buddy.getState() == Buddy.STATE_RECEIVED) {
                        requestsReceived.add(buddy);
                    } else if(buddy.getState() == Buddy.STATE_SENT) {
                        requestsSent.add(buddy);
                    } else {
                        buddies.add(buddy);
                    }
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    Buddy buddy = dataSnapshot.getValue(Buddy.class);
                    if(buddy.getState() == Buddy.STATE_RECEIVED) {
                        requestsReceived.remove(buddy);
                    } else if(buddy.getState() == Buddy.STATE_SENT) {
                        requestsSent.remove(buddy);
                    } else {
                        buddies.remove(buddy);
                    }
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
    }

    private void setupRecyclerView() {
        adapter = new BuddyAdapter(requestsReceived, requestsSent, buddies);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void onBuddyClick(View view) {
        Buddy buddy = (Buddy) view.getTag();
        switch (buddy.getState()) {
            case Buddy.STATE_RECEIVED:
                // Update buddy record in Firebase
                buddy.setState(Buddy.STATE_ACCEPTED);
                ref.child(BuddyApplication.getInstance().getUser().getUid()).child(buddy.getUid()).setValue(buddy);
                // Update corresponding buddy record in Firebase
                Buddy buddyTwo = new Buddy();
                buddyTwo.setUid(buddy.getUid());
                buddyTwo.setBuddyUid(BuddyApplication.getInstance().getUser().getUid());
                buddy.setState(Buddy.STATE_ACCEPTED);
                ref.child(buddy.getBuddyUid()).child(buddyTwo.getUid()).setValue(buddyTwo);
                break;
            case Buddy.STATE_SENT:

                break;
            case Buddy.STATE_ACCEPTED:

                break;
        }
    }

}
