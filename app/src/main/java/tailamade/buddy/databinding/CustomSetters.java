package tailamade.buddy.databinding;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import tailamade.buddy.Constants;
import tailamade.buddy.R;
import tailamade.buddy.models.Buddy;
import tailamade.buddy.models.User;

/**
 * Created by tomtaila on 4/2/16.
 */
public class CustomSetters {


    @BindingAdapter({"bind:glideSrc", "bind:glideTransformation"})
    public static void setImageUrl(final ImageView imageView, String imageSrc, int glideTransformation) {
        final Context context = imageView.getContext();
        switch (glideTransformation) {
            case Constants.GLIDE_TRANSFORMATION_CIRCULAR:
                Glide.with(context).load(imageSrc).bitmapTransform(new CropCircleTransformation(context)).crossFade().into(imageView);
                break;
            case Constants.GLIDE_TRANSFORMATION_ROUNDED_CORNERS:
                Glide.with(context).load(imageSrc).bitmapTransform(new CenterCrop(context), new RoundedCornersTransformation(context, 8, 8)).crossFade().into(imageView);
                break;
            default:
                Glide.with(context).load(imageSrc).centerCrop().crossFade().into(imageView);
                break;
        }
    }

}
