package tailamade.buddy;

import android.Manifest;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.firebase.client.Firebase;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import tailamade.buddy.databinding.ActivityProfileBinding;
import tailamade.buddy.models.User;

/**
 * Created by tomtaila on 4/2/16.
 */
@RuntimePermissions
public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = "ProfileActivity";

    User user;

    private Firebase ref = new Firebase(Constants.FIREBASE_BASE);
    private Firebase userRef;

    @Bind(R.id.et_name)
    EditText etName;
    @Bind(R.id.et_age)
    EditText etAge;
    @Bind(R.id.et_bio)
    EditText etBio;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = BuddyApplication.getInstance().getUser();
        ActivityProfileBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        binding.setUser(user);
        ButterKnife.bind(this);

        ProfileActivityPermissionsDispatcher.setupGoogleApiClientManagerWithCheck(this);

        userRef = ref.child(Constants.FIREBASE_CHILD_USERS).child(user.getUid());
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    void setupGoogleApiClientManager() {
        GoogleApiClientManager.getInstance().buildApiClient(this);
    }

    @OnClick(R.id.btn_save) void updateUser() {
        Log.d(TAG, "Update user");
        user.setName(etName.getText().toString());
        user.setAge(Integer.valueOf(etAge.getText().toString()));
        user.setBio(etBio.getText().toString());
        userRef.setValue(user);
    }

    @OnClick(R.id.btn_logout) void logout() {
        switch (ref.getAuth().getProvider()) {
            case Constants.PROVIDER_FB:
                LoginManager.getInstance().logOut();
                ref.unauth();
                break;
        }
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(GoogleApiClientManager.getInstance().getApiClient() != null) GoogleApiClientManager.getInstance().getApiClient().connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(GoogleApiClientManager.getInstance().getApiClient() != null) GoogleApiClientManager.getInstance().getApiClient().disconnect();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        ProfileActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    void showRationaleForLocation(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_location_rationale)
                .setPositiveButton(R.string.allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    void showDeniedForLocation() {
        Toast.makeText(this, "Sorry buddy, geo location is essential to the funcionality of this app!", Toast.LENGTH_SHORT);
        ProfileActivityPermissionsDispatcher.setupGoogleApiClientManagerWithCheck(this);
    }

}
