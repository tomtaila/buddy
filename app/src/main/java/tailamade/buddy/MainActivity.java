package tailamade.buddy;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.facebook.login.LoginManager;
import com.firebase.client.Firebase;

import butterknife.ButterKnife;
import butterknife.OnClick;
import tailamade.buddy.models.User;

/**
 * Created by tomtaila on 4/9/16.
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    Firebase ref = new Firebase(Constants.FIREBASE_BASE);
    User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        user = BuddyApplication.getInstance().getUser();
    }

    @OnClick(R.id.btn_users_activity) void startFindUsersActivity() {
        Intent intent = new Intent(this, FindUsersActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_logout) void logout() {
        if(ref.getAuth() != null) {
            switch (ref.getAuth().getProvider()) {
                case Constants.PROVIDER_FB:
                    LoginManager.getInstance().logOut();
                    ref.unauth();
                    BuddyApplication.getInstance().logout();
                    break;
            }
        }
    }

    @OnClick(R.id.btn_profile_activity) void startProfileActivity() {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_buddies) void startBuddiesActivity() {
        Intent intent = new Intent(this, BuddiesActivity.class);
        startActivity(intent);
    }

}
